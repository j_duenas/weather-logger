/*
Name:		weatherlogger.ino
Created:	3/28/2017 7:17:27 PM
Author:	Jeremy
*/
#include <Adafruit_Sensor.h>
#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <DHT.h>

// Wifi Variables
const char* wifiSsid = "Duenet";
const char* wifiPass = "pmJS314lc";
// Key and URL Variables
String secretKey = "3XhV7eJW2bWJcawLNt4Cb2Ap577p83";
String url = "http://projects.duenet.xyz/weather_logger/index.php?key=" + secretKey;
// Timing Variables
float intervalLengthInHours = .25; //How many hours between logData runs? default 3
unsigned long previousMillis = 0;
const long interval = intervalLengthInHours * 3600 * 1000; //hours * 60 minutes to an hour * 60 seconds to a minute * 1000 miliseconds to a second.

// DHT Initialization and Temp/Humidity variables.
DHT dht(D4, DHT11, 11);

void setup() {
    // Attempt WiFi Connection
    Serial.begin(74880);
    Serial.printf("\nConnecting to %s ", wifiSsid);
    WiFi.begin(wifiSsid, wifiPass);
    while (WiFi.status() != WL_CONNECTED) {
        Serial.print(".");
        delay(500);
    }
    Serial.println("WiFi Connected | IP Address: " + String(WiFi.localIP()));
    // Initial run of logData before delay
    logData();
}

void loop() {
    // Grab current 'time', check it against interval, run logData if it's time.
    unsigned long currentMillis = millis();
    if (currentMillis - previousMillis >= interval) {
        previousMillis = currentMillis;
        logData();
    }
}

void logData() {
    // Initialize Http
    HTTPClient http;

    // Read temp sensors.
    String fetchData(String type);
    delay(3000);
    String temperature = fetchData("temperature");
    delay(3000);
    String humidity = fetchData("humidity");

    // Compile sensor data and make GET request
    String finalURL = url + "&temperature=" + temperature + "&humidity=" + humidity + "&location=attic";
    http.begin(finalURL);
    Serial.println(finalURL);
    int httpCode = http.GET();
    if (httpCode > 0) {
        Serial.printf("[HTTP] GET.... code: %d\n", httpCode);
        // if GET request ok, dump out the contents
        if (httpCode == HTTP_CODE_OK) {
            String payload = http.getString();
            Serial.println("\n" + payload);
        }
    }
    else {
        Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
    }

    http.end();
}

String fetchData(String type = "temperature") {
    if (type == "temperature") {
        float temperature{ dht.readTemperature(true) };
        if (isnan(temperature)) {
            Serial.println("Temperature sensor read error!");
            delay(500);
            return "ERROR_T";
        }
        else {
            return String(temperature);
        }
    }
    else {
        float humidity{ dht.readHumidity(true) };
        if (isnan(humidity)) {
            Serial.println("Humidity sensor read error!");
            delay(500);
            return "ERROR_H";
        }
        else {
            return String(humidity);
        }
    }
}