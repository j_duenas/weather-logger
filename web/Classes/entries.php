<?php
	namespace weatherlogger;

	class entries{
		public $total;
		protected $dbPath = "data.sqlite";
		protected $dbConn;

		public function __construct(){
			$this->dbConn = new \PDO('sqlite:' . $this->dbPath);
		}

		public function getEntries($location = "attic", $total = -1){
			if($total === -1){
				$sql = "SELECT * FROM weather_entries WHERE location = :location ORDER BY id DESC";
			} else{
				$sql = "SELECT * FROM weather_entries WHERE location = :location LIMIT :total ORDER BY id DESC";
			}
			
			$stmt = $this->dbConn->prepare($sql);
			$stmt->bindParam(':location', $location, \PDO::PARAM_STR);

			if($total !== -1){
				$stmt->bindParam(':total', $total, \PDO::PARAM_INT);
			}

			$stmt->execute();
			return $stmt->fetchAll();

		}
	}