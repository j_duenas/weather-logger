<?php
	namespace weatherlogger;
	
	class key{
		protected $key;

		public function __construct($security_key = "password"){
			$this->key = $security_key;
		}

		public function verify($providedKey){
			return ($providedKey === $this->key) ? true : false;
		}
	}