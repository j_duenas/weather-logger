<?php
	namespace weatherlogger;

	class log{
		protected $dbPath = "data.sqlite";
		protected $dbConn;
		public $temperature;
		public $humidity;
		public $date;
		public $location;

		public function __construct(){
            date_default_timezone_set('America/New_York');
		    $this->date = date('r');
			$this->dbConn = new \PDO('sqlite:' . $this->dbPath);
		}

		public function addEntry($temperature, $humidity, $location){
		    $this->temperature = $temperature;
		    $this->humidity = $humidity;
		    $this->location = $location;

            $sql = "INSERT INTO weather_entries(datetime, temperature, humidity, location) VALUES(:datetime, :temperature, :humidity, :location)";
            $stmt = $this->dbConn->prepare($sql);
            $stmt->bindParam(':datetime', $this->date);
            $stmt->bindParam(':temperature', $this->temperature);
            $stmt->bindParam(':humidity', $this->humidity);
            $stmt->bindParam(':location', $this->location);
            $stmt->execute();
        }
	}

