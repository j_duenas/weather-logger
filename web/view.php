<?php
	namespace weatherlogger;

	require 'Classes/key.php';
	require 'Classes/log.php';
	require 'Classes/entries.php';

	// Verify Key
	$securityKey = new key("LnWyMB2TWHGfgZUjzTj83TYfvL222c");
	if( !$securityKey->verify($_GET['key'])){
		die("Invalid Key!");
	}

	$entries = new Entries;
	$attic_entries = $entries->getEntries("attic");
	$basement_entries = $entries->getEntries("basement");

	date_default_timezone_set('America/New_York');
?>
	<!doctype html>
	<html>
	<head>
		<style type="text/css">
			body{
				font-size: .8em;
			}
			table{
				border-collapse: collapse;
				width: 100%;
			}

			td{
				padding: 10px;
			}
			tr:nth-child(odd) td{
				background-color: #ccc;
			}

			#container{
				display: flex;
			}
				#container>div{
					width: 33.33%;
					padding: 0 1%;
				}

			.red{
				color: red;
				font-weight: bold;
			}

			.green{
				color: green;
				font-weight: bold;
			}
		</style>
	</head>
	<body>
		<h1>Weather Logger</h1>
		<div id="container">
			<div>
				<h2>Atop my second monitor</h2>
				<table>
					<tr>
						<td>Date/Time</td>
						<td>Temperature</td>
						<td>Humidity</td>
					</tr>
				<?php
					$last_temp_entry = 0;
					$last_temp_entry = 0;

					foreach($attic_entries as $entry){
						$temperature_dir = ($entry['temperature'] > $last_temp_entry) ? "<span class='green'>+</span>" : "<span class='red'>-</red>";
						$humidity_dir = ($entry['humidity'] > $last_humidity_entry) ? "<span class='green'>+</span>" : "<span class='red'>-</span>";

						$last_temp_entry = $entry['temperature'];
						$last_humidity_entry = $entry['humidity'];
				?>
					<tr>
						<td>
							<?php echo date('m/d/y @ h:i a', strtotime($entry['datetime']));?>
						</td>
						<td><?php echo $entry['temperature'];?> <?php //echo $temperature_dir;?></td>
						<td><?php echo $entry['humidity'];?> <?php //echo $humidity_dir;?></td>
					</tr>
				<?php		
					}
				?>
				</table>
			</div>
			<div>
				<!-- <h2>Basement</h2>
				<table>
					<tr>
						<td>Date/Time</td>
						<td>Temperature</td>
						<td>Humidity</td>
					</tr>
				<?php
					foreach($basement_entries as $entry){
				?>
					<tr>
						<td>
							<?php echo date('m/d/y @ h:i a', strtotime($entry['datetime']));?>
						</td>
						<td><?php echo $entry['temperature'];?></td>
						<td><?php echo $entry['humidity'];?></td>
					</tr>
				<?php		
					}
				?>
				</table> -->
			</div>
		</div>
	</body>
	</html>